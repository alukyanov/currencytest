from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from main.app.views import IndexView, user_currency_list, append_currency_pair,\
    update_rate


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^$', login_required(IndexView.as_view())),
    url(r'^API/GetUserCurrencies$', login_required(user_currency_list),
        name='user-currencies'),
    url(r'^API/AppendCurrencyPair$', login_required(append_currency_pair),
        name='append-currency-pair'),
    url(r'^API/UpdateRate$', login_required(update_rate),
        name='update-rate'),
]
