import json
import logging
import requests

from .models import Currency


log = logging.getLogger('file')


class CurrencyAPI(object):

    @classmethod
    def _callback(cls, resp):
        try:
            data = json.loads(resp.content)
        except Exception as e:
            log.warning(
                "An error occurred while fetching the currency rate: {}"
                "".format(e))

        else:
            ticker = data['ticker']
            try:

                base = ticker['base'].upper()
                target = ticker['target'].upper()
                currency_hash = Currency.api.get_currency_hash(base, target)
                currency = Currency.api.get_or_create(
                    currency_hash=currency_hash,
                    currency_from=base,
                    currency_to=target,
                )[0]
                currency.rate = float(ticker['price'])
                currency.save()

            except Exception as e:
                log.warning(
                    "An error occurred while fetching the currency rate: {}"
                    "".format(e))

    @classmethod
    def update_rate(cls, urls):
        for url in urls:
            resp = requests.get(url)
            cls._callback(resp)
