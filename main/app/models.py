from datetime import datetime, timedelta
from hashlib import md5
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


class CurrencyManager(models.Manager):
    def get_currency_hash(self, cur_from, cur_to):
        s = '{}-{}'.format(cur_from, cur_to)
        s = s.encode('utf8')
        return md5(s).hexdigest()

    def get_by_pair(self, cur_from, cur_to):
        return self.get_or_create(
            currency_hash=self.get_currency_hash(cur_from, cur_to),
            currency_from=cur_from,
            currency_to=cur_to
        )[0]


class Currency(models.Model):
    updated = models.DateTimeField(auto_now=True)
    currency_hash = models.CharField(max_length=128)
    currency_from = models.CharField(max_length=255)
    currency_to = models.CharField(max_length=255)
    rate = models.FloatField(null=True, blank=True)

    api = CurrencyManager()

    class Meta:
        ordering = ('currency_from', 'currency_to')

    def __str__(self):
        return '{} - {}: {}'.format(
            self.currency_from, self.currency_to, self.rate or '-')

    @property
    def pair(self):
        return '{} - {}'.format(self.currency_from, self.currency_to)

    def to_dict(self):
        return {
            'hash': self.currency_hash,
            'base': self.currency_from,
            'target': self.currency_to,
            'price': self.rate,
            'updated': self.updated.isoformat(sep=' ')[:19]
        }


class UserCurrency(models.Model):
    user = models.ForeignKey(User)
    currencies = models.ManyToManyField(Currency)

    class Meta:
        ordering = ('user',)

    def __str__(self):
        currency_pairs_list = self.currencies.select_related()
        return '<User: {} [{}]>'.format(self.user.username, ', '.join([
            c.pair for c in currency_pairs_list
        ]))

    def get_expired(self):
        currency_hash_list = [
            c.currency_hash
            for c in self.currencies.select_related()
        ]
        return Currency.api.filter(
            currency_hash__in=currency_hash_list
        ).filter(
            models.Q(
                updated__lte=datetime.now() - timedelta(
                    seconds=settings.DEFAULT_EXPIRED_SECONDS)
            ) | models.Q(rate=None)
        )

    def get_currencies(self):
        return [c.to_dict() for c in self.currencies.select_related()]
