import logging
from django.http import JsonResponse
from django.conf import settings
from django.views.generic import TemplateView
from django.core.exceptions import ObjectDoesNotExist

from .models import UserCurrency, Currency
from .utils import CurrencyAPI


log = logging.getLogger('file')


class IndexView(TemplateView):
    template_name = 'index.html'


def user_currency_list(request):
    """
    
    :param request: 
    :return: JsonResponse 
    """
    try:

        user_currency = UserCurrency.objects.get(user=request.user)

    except ObjectDoesNotExist:
        default_currency_pair = settings.DEFAULT_CURRENCY_PAIR
        cur_pair = Currency.api.get_by_pair(*default_currency_pair)
        user_currency = UserCurrency.objects.create(user=request.user)
        user_currency.currencies.add(cur_pair)
        user_currency.save()

    except Exception as e:
        return JsonResponse(dict(error=str(e)))

    return JsonResponse(dict(data=user_currency.get_currencies()))


def append_currency_pair(request):
    data = request.POST.copy()

    try:
        base = data['base']
        target = data['target']

        currency = Currency.api.get_by_pair(base, target)
        user_currency = UserCurrency.objects.get_or_create(user=request.user)[0]
        user_currency.currencies.add(currency)
        user_currency.save()

        urls = [settings.DEFAULT_CURRENCY_SERVICE_API['RATE'].format(
            base.upper(), target.upper())]
        CurrencyAPI.update_rate(urls)
    except Exception as e:
        return JsonResponse(dict(error=str(e)))

    else:
        return JsonResponse(dict(status=True))


def update_rate(request):
    try:

        expired_currencies_list = UserCurrency.objects.get(
            user=request.user).get_expired()

        urls = []
        api_url = settings.DEFAULT_CURRENCY_SERVICE_API['RATE']
        for c in expired_currencies_list:
            urls.append(api_url.format(c.currency_from, c.currency_to))
        CurrencyAPI.update_rate(urls)

    except Exception as e:
        return JsonResponse(dict(error=str(e)))

    else:
        return JsonResponse(dict(status=True))
