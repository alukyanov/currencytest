from django.contrib.auth.models import User
from django.conf import settings
from django.test import TestCase

from main.app.models import UserCurrency, Currency
from main.app.utils import CurrencyAPI


class CurrencyTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test')
        self.user.set_password('test')
        self.user.save()
        self.uc = UserCurrency.objects.get_or_create(user=self.user)[0]

    def test_init_user(self):
        base = 'BTC'
        target = 'USD'
        c = Currency.api.get_by_pair(base, target)
        self.uc.currencies.add(c)
        self.uc.save()

        urls = []
        api_url = settings.DEFAULT_CURRENCY_SERVICE_API['RATE']
        expired_currencies_list = UserCurrency.objects.get(
            user=self.user).get_expired()

        for c in expired_currencies_list:
            urls.append(api_url.format(c.currency_from, c.currency_to))

        CurrencyAPI.update_rate(urls)

        self.assertIsNotNone(self.uc.get_currencies()[0].get('price'))
