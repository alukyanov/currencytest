from django.contrib import admin

from .models import Currency, UserCurrency


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('currency_from', 'currency_to', 'rate', 'updated',
                    'currency_hash')


@admin.register(UserCurrency)
class UserCurrencyAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user')
