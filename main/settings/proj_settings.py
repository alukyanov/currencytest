DEFAULT_CURRENCY_PAIR = ('BTC', 'USD')
DEFAULT_CURRENCY_SERVICE_API = {
    'CURRENCIES': 'https://www.cryptonator.com/api/currencies',
    'RATE': 'https://api.cryptonator.com/api/full/{}-{}'
}
DEFAULT_EXPIRED_SECONDS = 10
